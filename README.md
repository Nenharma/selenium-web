## Maven profiles
You can change profiles with;
-P headlessOFF
-P headlessON

When you change to headlessON, test run at browsers which is working with headless option.
When you change to headlessON, test run at browsers which is working without headless option.

PS: Default profile is headlessON

---

## Running tests

You can start tests with;
mvn clean test

---

## Create a test report

Test results created under ./target/allure-results folder.

You can generate report with;
allure serve ./target/allure-results

This command'll open a html file(index) with your default browser.


---

## Running only one test class
You can run only one test class with;

mvn -Dtest={ClassName} test

## Example of running one test class with a headlessOFF profile and after that creating test report.

**First**

mvn -Dtest=StoreTest clean test -P headlessOFF

**After test execution finished**

allure serve ./target/allure-results



**All these commands needs to run at same directory with the project**
