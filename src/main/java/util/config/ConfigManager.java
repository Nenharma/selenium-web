package util.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigManager {

    private InputStream inputStream;

    private String chrome = "chrome";
    private boolean chromeValue;

    private String firefox = "firefox";
    private boolean firefoxValue;

    public ConfigManager() {

        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader()
                    .getResourceAsStream("config.properties");

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            chromeValue = Boolean.valueOf(prop.getProperty(chrome));
            firefoxValue = Boolean.valueOf(prop.getProperty(firefox));

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean getFirefoxHeadlessOption(){
        return firefoxValue;
    }
    public boolean getChromeHeadlessOption(){
        return chromeValue;
    }
}