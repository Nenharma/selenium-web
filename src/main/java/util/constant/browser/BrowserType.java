package util.constant.browser;

public enum BrowserType {
    Chrome("chrome"),
    Firefox("firefox");

    private String name;

    BrowserType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
