package util.constant.browser;

import org.openqa.selenium.firefox.FirefoxOptions;
import util.config.ConfigManager;

public class Firefox {

    FirefoxOptions firefoxOptions = new FirefoxOptions();
    ConfigManager configManager = new ConfigManager();

    public Firefox(){
        firefoxOptions.setHeadless(configManager.getFirefoxHeadlessOption());
    }

    public FirefoxOptions getOptions() {
        return firefoxOptions;
    }
}
