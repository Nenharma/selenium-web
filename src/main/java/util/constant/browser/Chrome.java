package util.constant.browser;

import org.openqa.selenium.chrome.ChromeOptions;
import util.config.ConfigManager;

public class Chrome {

    ConfigManager configManager = new ConfigManager();
    ChromeOptions options = new ChromeOptions();

    public Chrome(){
        options.addArguments("--incognito");
        options.addArguments("--start-maximized");
        if(configManager.getChromeHeadlessOption()) {
            options.addArguments("--headless");
        }
    }

    public ChromeOptions getOptions() {
        return options;
    }
}
