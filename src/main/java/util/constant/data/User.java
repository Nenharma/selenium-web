package util.constant.data;

public class User {

    public static class UserInformation{

        public String name;
        public String phone;
        public String email;
        public String reference;
        public String company;

        public UserInformation(){
            name = "TestUSer";
            phone = "1234567890";
            email = "testuser@test.test";
            reference = "Alpha";
            company = "Takeaway";
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }
    }

    public static class UserAddress {

        public String addressDetail;
        public String postalCode;
        public String cityName;

        public UserAddress() {
            addressDetail = "main street 2415";
            postalCode = "8888AA";
            cityName = "Enschede";
        }

        public String getAddressDetail() {
            return addressDetail;
        }

        public void setAddressDetail(String addressDetail) {
            this.addressDetail = addressDetail;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }
    }
}