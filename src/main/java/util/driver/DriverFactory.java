package util.driver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import util.constant.browser.BrowserType;
import util.constant.browser.Chrome;
import util.constant.browser.Firefox;

public class DriverFactory {

    static RemoteWebDriver driver;

    public static RemoteWebDriver createInstance(BrowserType browserType) throws Exception {

        if (browserType == BrowserType.Chrome){
            System.setProperty("webdriver.chrome.driver", "chromedriver");
            Chrome chrome = new Chrome();
            driver = new ChromeDriver(chrome.getOptions());
        } else if (browserType == BrowserType.Firefox) {
            System.setProperty("webdriver.gecko.driver", "geckodriver");
            Firefox firefox = new Firefox();
            driver = new FirefoxDriver(firefox.getOptions());
        } else {
            System.out.println("Driver type is not selected.");
            driver = null;
        }
        return driver;
    }
}
