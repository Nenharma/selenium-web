package util.driver;

import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DriverManager {

    private static ThreadLocal<String> driverError = new ThreadLocal<String>();

    private static ThreadLocal<RemoteWebDriver> webDriver = new ThreadLocal<RemoteWebDriver>();

    private static List<RemoteWebDriver> driversToCleanup = Collections.synchronizedList(new ArrayList<RemoteWebDriver>());

    public static RemoteWebDriver getDriver() {
        return webDriver.get();
    }

    public static List<RemoteWebDriver> getDriversToCleanup(){return driversToCleanup;}

    public static void addDriverToDriversToCleanUp(RemoteWebDriver remoteWebDriver){driversToCleanup.add(remoteWebDriver);}

    public static void setWebDriver(RemoteWebDriver driver) {
        webDriver.set(driver);
    }

    public static String getDriverError() {
        return driverError.get();
    }

    public static void setDriverError(String error) {
        driverError.set(error);
    }
}
