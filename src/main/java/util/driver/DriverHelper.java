package util.driver;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class DriverHelper {

    @Attachment(value = "{testName} - screenshot", type = "image/png")
    public static byte[] captureScreen() {

        return ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
    }
}
