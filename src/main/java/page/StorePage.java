package page;

import base.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class StorePage extends BasePage {

    private By mealContainer = By.className("menucard-meal__sidedish-button");
    private By addButton = By.className("add-btn-icon");
    private By orderButton = By.className("cartbutton-button");

    @Step("Selecting first item in store page")
    public StorePage selectFirstItem() {
        click(mealContainer);
        return this;
    }

    @Step("Adding item to basket")
    public StorePage addItemToBasket() {
        click(addButton);
        return this;
    }

    @Step("Clicking order button")
    public OrderPage clickOrderButton() {
        click(orderButton);
        return new OrderPage();
    }
}
