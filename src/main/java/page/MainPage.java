package page;

import base.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MainPage extends BasePage {

    private String baseURL = "https://www.thuisbezorgd.nl/en";
    private By searchBox = By.id("imysearchstring");
    private By showButton = By.id("submit_deliveryarea");
    private By references = By.id("reference");

    @Step("Navigating to main page")
    public MainPage navigateToMainPage() {
        navigateTo(baseURL);
        return this;
    }

    @Step("Writing address into searchbox")
    public MainPage writeAddress(String addressText) {
        write(searchBox, addressText);
        return this;
    }

    @Step("Clicking show button")
    public MainPage clickShowButton() {
        try {
            Thread.sleep(2000);
            click(showButton);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Step("Select address from multiple choices")
    public SearchPage selectAddress(String referenceChoice) {
        List<WebElement> referenceList = waitAndReturnElements(references);

        for(WebElement reference : referenceList){
            if (reference.getText().contains(referenceChoice)){
                reference.click();
                break;
            }
        }
        return new SearchPage();
    }
}
