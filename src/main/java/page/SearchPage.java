package page;

import base.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class SearchPage extends BasePage {

    private By restaurants = By.className("restaurant");
    private By firstResaurants = By.id("irestaurantN311515");
    private By title = By.className("topbar__title");

    @Step("Checking if stores visible")
    public boolean isStoresVisible() {
        waitAndReturnElement(title);
        return getSizeOfElementList(restaurants)>0;
    }

    @Step("Selecting first store")
    public StorePage selectFirstStore() {
        click(firstResaurants);
        return new StorePage();
    }
}
