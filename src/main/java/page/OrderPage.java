package page;

import base.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class OrderPage extends BasePage {

    private By addressTextBox = By.id("iaddress");
    private By cityTextBox = By.id("itown");
    private By nameTextBox = By.id("isurname");
    private By emailTextBox = By.id("iemail");
    private By phoneTextBox = By.id("iphonenumber");
    private By companyNameTextBox = By.id("icompanyname");
    private By orderAndPayButton = By.className("checkout-orderbutton-btn");

    @Step("Writing address description")
    public OrderPage writeAddressText(String addressText) {
        write(addressTextBox, addressText);
        return this;
    }

    @Step("Writing postal code")
    public OrderPage writePostalCode() {
        return this;
    }

    @Step("Writing city name")
    public OrderPage writeCity(String cityName) {
        write(cityTextBox, cityName);
        return this;
    }

    @Step("Writing user name")
    public OrderPage writeName(String name) {
        write(nameTextBox, name);
        return this;
    }

    @Step("Writing user email")
    public OrderPage writeEmail(String email) {
        write(emailTextBox, email);
        return this;
    }

    @Step("Writing user phone number")
    public OrderPage writePhoneNumber(String phoneNumber) {
        write(phoneTextBox, phoneNumber);
        return this;
    }

    @Step("Writing company name")
    public OrderPage writeCompanyName(String companyName) {
        write(companyNameTextBox, companyName);
        return this;
    }

    @Step("Writing remarks")
    public OrderPage writeRemarks() {
        return this;
    }

    @Step("Selecting order amount")
    public OrderPage selectOrderAmount() {
        return this;
    }

    @Step("Clicking order and pay button")
    public OrderDetailPage clickOrderAndPay() {
        scrollDown();
        click(orderAndPayButton);
        return new OrderDetailPage();
    }
}
