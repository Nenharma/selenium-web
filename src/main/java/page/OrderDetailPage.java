package page;

import base.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class OrderDetailPage extends BasePage {

    private By reference = By.className("order-purchaseid");

    @Step("Getting order reference number for logging and further usages")
    public String getOrderReference() {
        scrollDown();
        return waitAndReturnElement(reference).getText();
    }

    @Step("Checking if order reference number generating")
    public boolean checkOrderReferenceNumber() {
        return isElementPresentAndDisplay(reference);
    }
}
