package base;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.driver.DriverManager;

import java.util.Date;
import java.util.List;

public class BasePage {

    protected final int TIME_OUT_SHORT = 5;
    protected final int TIME_OUT_MEDIUM = 10;
    protected final int TIME_OUT_LONG = 20;

    public void navigateTo(String url){
        DriverManager.getDriver().navigate().to(url);
    }

    public void click(By selector) {
        try {
            WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), TIME_OUT_LONG);
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
            element.click();
        } catch (StaleElementReferenceException e){}
    }

    public void write(By selector, String text) {
        try {
            WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), TIME_OUT_MEDIUM);
            WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
            element.clear();
            element.sendKeys(text);
        } catch (StaleElementReferenceException s) {}
    }

    public void scrollDown() {

        JavascriptExecutor jse = (JavascriptExecutor) DriverManager.getDriver();
        jse.executeScript(
                "window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
    }

    public void scrollUp() {

        JavascriptExecutor jse = (JavascriptExecutor) DriverManager.getDriver();
        jse.executeScript("scroll(0, 0);");
    }

    public boolean isElementPresentAndDisplay(By selector) {
        final long start = new Date().getTime();

        WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), TIME_OUT_MEDIUM);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(selector));

        do {
            try {
                if (element.isDisplayed()) {
                    return true;
                }
            } catch (Exception ignore) {
            }

            try {
                Thread.sleep(100);
            } catch (Exception ignore) {}
        } while (new Date().getTime() - start < TIME_OUT_SHORT * 1000);

        return false;
    }

    public WebElement waitAndReturnElement(By selector){
        WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), TIME_OUT_MEDIUM);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
        return element;
    }

    public List<WebElement> waitAndReturnElements(By selector){
        WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(), TIME_OUT_MEDIUM);
        List<WebElement> elements = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector));
        return elements;
    }

    public int getSizeOfElementList(By selector) {
        return DriverManager.getDriver().findElements(selector).size();
    }
}
