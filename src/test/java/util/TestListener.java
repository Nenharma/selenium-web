package util;

import com.automation.remarks.video.recorder.VideoRecorder;
import io.qameta.allure.Attachment;
import org.junit.Rule;
import org.junit.internal.AssumptionViolatedException;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import util.driver.DriverHelper;

public class TestListener {

    @Rule
    public TestRule classWatcher = new TestWatcher(){

        @Override
        protected void succeeded(Description description) {
            try {
                DriverHelper.captureScreen();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println("Test Success: " + description.getMethodName());
            super.succeeded(description);
        }

        @Override
        public Statement apply(Statement base, Description description) {
            return super.apply(base, description);
        }

        @Override
        @Attachment
        protected void starting(Description description) {
            VideoRecorder.conf().remoteUrl();
            System.out.println("Test Start: " + description.getMethodName());
            super.starting(description);
        }

        @Override
        protected void finished(Description description) {
            try {
                DriverHelper.captureScreen();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println("Test Finished: " + description.getMethodName());
            super.finished(description);
        }

        @Override
        protected void skipped(AssumptionViolatedException e, Description description) {
            try {
                DriverHelper.captureScreen();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println("Test Skipped: " + description.getMethodName());
            super.skipped(e, description);
        }

        @Override
        protected void failed(Throwable e, Description description) {
            try {
                DriverHelper.captureScreen();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            System.out.println("Test Failed:" + description.getMethodName());
            super.failed(e, description);
        }
    };
}