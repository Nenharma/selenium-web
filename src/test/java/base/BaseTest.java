package base;

import com.automation.remarks.junit5.VideoExtension;
import org.junit.jupiter.api.AfterAll;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.TestRule;
import org.openqa.selenium.remote.RemoteWebDriver;
import util.TestListener;
import util.constant.browser.BrowserType;
import util.driver.DriverFactory;
import util.driver.DriverManager;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@ExtendWith(VideoExtension.class)
public class BaseTest {

    public TestListener testListener = new TestListener();

    @Rule
    public TestRule classWatcher = testListener.classWatcher;

    public static Stream<RemoteWebDriver> drivers() throws Exception {
        List<RemoteWebDriver> drivers = new ArrayList<RemoteWebDriver>();
        drivers.add(DriverFactory.createInstance(BrowserType.Firefox));
        drivers.add(DriverFactory.createInstance(BrowserType.Chrome));
        DriverManager.addDriverToDriversToCleanUp(drivers.get(0));
        DriverManager.addDriverToDriversToCleanUp(drivers.get(1));
        return Stream.of(drivers.get(0), drivers.get(1));
    }

    @BeforeEach
    public void setUp(TestInfo info) throws Exception {

    }

    @AfterAll
    public static void tearDown() throws IOException {
        for (RemoteWebDriver driver: DriverManager.getDriversToCleanup()) {
            driver.quit();
        }
    }
}