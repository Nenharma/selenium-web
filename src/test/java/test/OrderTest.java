package test;

import base.BaseTest;
import com.automation.remarks.video.annotations.Video;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.remote.RemoteWebDriver;
import page.MainPage;
import page.OrderDetailPage;
import util.constant.data.User;
import util.driver.DriverManager;

public class OrderTest extends BaseTest {

    private MainPage page = new MainPage();
    private User.UserInformation userInformation = new User.UserInformation();
    private User.UserAddress userAddress = new User.UserAddress();

    // Positive test cases
    @Story("Success Order Case")
    @Description("In this test we will order a food from a store successfully")
    @Video(name = "order_success")
    @ParameterizedTest
    @MethodSource("drivers")
    public void orderSuccess(RemoteWebDriver driver) throws Exception {
        DriverManager.setWebDriver(driver);
        System.out.println("Order Success Test Case");
        String orderReference = page.navigateToMainPage().
                writeAddress(userAddress.getPostalCode()).
                clickShowButton().
                selectAddress(userInformation.getReference()).
                selectFirstStore().
                selectFirstItem().
                addItemToBasket().
                clickOrderButton().
                writeAddressText(userAddress.getAddressDetail()).
                writeCity(userAddress.getCityName()).
                writeName(userInformation.getName()).
                writeEmail(userInformation.getEmail()).
                writePhoneNumber(userInformation.getPhone()).
                writeCompanyName(userInformation.getCompany()).
                clickOrderAndPay().
                getOrderReference();

        boolean orderReferenceCheck = new OrderDetailPage().checkOrderReferenceNumber();

        System.out.println("Order Reference: " + orderReference);
        Assert.assertTrue(orderReferenceCheck);
    }
}
