package test;

import base.BaseTest;
import com.automation.remarks.video.annotations.Video;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.remote.RemoteWebDriver;
import page.MainPage;
import util.constant.data.User;
import util.driver.DriverManager;

public class StoreTest extends BaseTest {

    private MainPage page = new MainPage();
    private User.UserInformation userInformation = new User.UserInformation();
    private User.UserAddress userAddress = new User.UserAddress();

    // Positive test cases
    @Story("Success Store Case")
    @Description("In this test we will check are stores visible in search page")
    @Video(name = "store_success")
    @ParameterizedTest
    @MethodSource("drivers")
    public void checkStoreVisibility(RemoteWebDriver driver) throws Exception {
        DriverManager.setWebDriver(driver);
        boolean storeVisible = page.navigateToMainPage().
                writeAddress(userAddress.getPostalCode()).
                clickShowButton().
                selectAddress(userInformation.getReference()).
                isStoresVisible();

        Assert.assertTrue(storeVisible);
    }
}
